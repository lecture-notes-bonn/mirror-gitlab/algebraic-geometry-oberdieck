$X,Y$ Noetherian schemes, $\pi \colon X \to Y$ proper, $\F$ a coherent sheaf over $X$.
What is the relation between the fibers of $R^i_{\pi_\ast} \F$ (coherent, proof skipped, or assume $\pi$ is projective) and $H^i(X_y,\res{\F}{X_y})$ if $\F$ is not flat over $Y$ or the \Cref{CBC} does not apply?

Assume that $Y = \Spec B$.
Then $R^i_{\pi_\ast} \F = H^i(X,\F)^\sim$ (wrt $B$).

\begin{example}
  $\E = \MO_{\PR^1} \oplus \MO_{\PR^1}$,
  $C = \PR^1_k \times \A^1_k \to Y = \A^1_k$, $i \colon \E_1 = \MO(-d) \hookrightarrow \MO_{\PR^1} \oplus \MO_{\PR^1}$ (s.t. the quotient is locally free, so $\cong \MO(d)$).
  $\F = \E_{\A^1} = \MO_{\PR^1}(-d)t \oplus \MO^2_{\PR^1} t^2 \oplus \MO_{PR^1}^{\oplus 2} t^3 \oplus \dotsb$ as $\MO_{\PR^1}(t)$-modules ($t$ is the coordinate of $\A^1_k$).
  \begin{align*}
    \pi_\ast \F = t^2 \MO_{\PR^1}(t)^{\oplus 2} &\not \leftrightarrow H^0(X_0,\F_0) = H^0(X_0,\MO(-d) \oplus \MO(d)) = k^{d+1}  \\
    R^1_{\pi_\ast} \F = k_0^{\oplus(d-1)} &\leftrightarrow H^1(X_0,\F_0) = k^{d-1}
  \end{align*}
\end{example}
Assume $y \in Y$ is a closed point, that is a maximal ideal $m \subseteq B$.
Consider the $n$-th order infinitesimal neighborhood of $X$:
\begin{equation*}
  \begin{tikzcd}
    X_y \ar[d] \ar[r,symbol=\subseteq] & X_n \ar[d] \ar[r] &X  \\
    y \ar[r,symbol=\in] & \Spec (B/m^n) \ar[r,hookrightarrow] & Y \; ,
  \end{tikzcd}
\end{equation*}
$(R^i_{\pi_\ast} \F) \otimes B/m^n \to H^i(X_n,\res{\F}{X_n})$.

We know that
\begin{align*}
  (R^i_{\pi_\ast}\F) \otimes B/m^n =&H^i(X,\F) \otimes B/m^n  \\
                                   =& H^i(C^\bullet(\mathcal U , \F)) \otimes_B B/m^n  \\
  \to& H^i(C^\bullet(\mathcal U ,\F) \otimes_B B/m^n)\\
  =&H^i(C^\bullet(\res{\mathcal U}{X_n},\res{\F}{X_n}))  \\
  =&H^i(X_n,\F_n) \; .
\end{align*}
For $n=2$, we have $\res{\F}{X_2} = \E_{\A^1} \otimes_{\MO_{\PR^1}(t)} \MO_{\PR^1}(t)/(t^2=0) = \MO_{\PR^1}(-d)t \oplus \MO_{\PR^1}^2t^2 \oplus \MO(d)t^3 \oplus \dotsb$.
We have $H^0(X_2,\res{\F}{X_2}) = k^2 t^2 \oplus k^{d+1} t^3$ ($\cdot t$), so we have a match with $(\pi_\ast \F)\otimes k[t]/t^2 = (k[t]/t^2)^{\oplus 2} = k^2 \oplus k^2 t$.
And we have $H^1(X_2,\res{\F}{X_2}) = k^{d-1}$.

For $n=3$:
$\res{\F}{X_3} \cong \MO_{\PR^1}(d)t \oplus \MO_{\PR^1}^2 t^2 \oplus \MO_{\PR^1} t^3 \oplus \MO_{\PR^1} (d)$, $H^0 = k^2t^2 \oplus k^2 t^3 \oplus k^{d+1} t^n$, and a match with $(\pi_\ast \F) \otimes (t^3 = 0) = k^2 \oplus k^2t \oplus k^2 t^2$ (in the first two components??).

$n \to \infty$:
$\varprojlim H^0(X_n,\F_n) = t^2 k[[t]]^{\oplus 2} \xleftarrow{\varprojlim r_{t=0}} (\pi_\ast \F)^{\wedge}_{t=0} = k[[t]]^{\oplus 2}$.
\begin{theorem}[of formal functions]
  The inverse limit of the morphism ${r_n}$ of inverse systems:
  \begin{equation*}
    \varprojlim_n r_n \colon (R^i_{\pi_\ast}\F)^1_y \to \varprojlim_n H^i(X_n,\res{\F}{X_n})
  \end{equation*}
  is an isomorphism for all $i$.
  (LHS completion at $y$).
\end{theorem}
\begin{remark}
  The same theorem holds for $y$ nonclosed.
  Define $X_n$ by
  \begin{equation*}
    \begin{tikzcd}
      X_n \ar[d] \ar[r] & X \ar[d]  \\
      \Spec(\MO_{Y,y}/m_y^n) \ar[r] &Y
    \end{tikzcd}
  \end{equation*}
\end{remark}
\begin{proof}
  See Vakil/Hartshorne, decreasing induction on $i$ ($\pi$ projective), Artin-Rees lemma.
\end{proof}
\section{Structure Theorems for Proper Morphisms}
\begin{definition}[$\MO$-connected]
  $f \colon X \to Y$ is \textbf{$\MO$-connected} if $f_\ast \MO_X = \MO_Y$.
\end{definition}
\begin{example}
  $f \colon X \to Y$ proper morphism of Noetherian schemes, flat over $k$,
  $h^0(X_t,\MO_{X_t}) = 1$ for all $t$, then $f$ is $\MO$-connected.
  (E.g. familiy of connected projective curves over $k = \bar k$, $h^0(C_t,\MO_t) = 1$).

  \begin{equation*}
    \begin{tikzcd}
      k = \MO_Y \otimes k(y) \ar[r,hookrightarrow] \ar[rr,bend right, "\cong"] & (\pi_\ast \MO_X) \otimes k(y) \ar[r,"r_y^0"] & H^0(X_y,\MO_{X_y}) = k
    \end{tikzcd}
  \end{equation*}
  so $r_y^0$ is surhective, so CBC gives that $r_y^0$ is an iso, so CBC implies that $\pi_\ast \MO_X$ is locally free, so $\pi_\ast \MO_X \cong \MO_Y$.
\end{example}
\begin{proposition}
  Let $f \colon X \to Y$ be a proper morphism of Noetherian schemes.
  Then if $f$ is $\MO$-connected, then for all $y \in Y$, $X_y = f^{-1}(y)$ is connected.
\end{proposition}
\begin{proof}
  As topological spaces, $X_n \cong X_1 = X_y$.
  Assume that $Y$ is affine, $y \in Y$ closed point (for simplicity).
  If $X_y = Z_1 \sqcup Z_2$, then
  \begin{align*}
    \varprojlim_n H^0(X_n,\MO_{X_n}) &= \varprojlim_n (H^0(Z_1,\MO_{X_n}) \times H^0(Z_2,\MO_{X_n}))  \\
                                     &= \varprojlim_n (H^0(Z_1,\MO_{X_n})) \times \varprojlim_n (H^0(Z_2,\MO_{X_n}))  \\
                                     &= R_1 \times R_2 \; .
  \end{align*}
  If $f$ is $\MO$-connected, then $\hat {\MO_{Y,y}} \cong R_1 \times R_2 \ni e_1,e_2$.
  Let $e_i^(n) \in H^0(X_n,\MO_{X_n})$ such that $\res{e_i^(n)}{Z_j} = \delta_{ij}$.
  Let $e_i \cong \varprojlim_n e_i^{(n)}$, then $e_1 \in R_1$, $e_2 \in R_2$, $e_1 + e_2 = 1$, $e_1 \cdot e_2 =0$.
  $\MO_{Y,y}$ is local ($e_1,e_2 \in m$), so $\hat{\MO_{Y,y}}$ is local, which is a contradiction.
\end{proof}

\begin{theorem}[Stein factorization]
  Let $\pi \colon X \to Y$ be a proper morphism of Noetherian schemes.
  There exists a canonical factorization

  \begin{equation*}
    \begin{tikzcd}
      X \ar[dr,"\pi"'] \ar[rr,"\text{$\alpha$ proper}","\text{$\MO$-connected}"'] && Y \ar[dl, "\text{$\beta$ proper}"]  \\
      &Y \; .
    \end{tikzcd}
  \end{equation*}
\end{theorem}
\begin{proof}
  $\pi$ is proper, therefore $\pi_\ast \MO_X$ is a coherent sheaf on $Y$.
  Therefore $\pi_\ast \MO_X$ is a finite $\MO_Y$-algebra.
  Consider $Y^\prime := \underline{\Spec}_{\MO_Y}(\pi_\ast \MO_X)$.
  There then exists a finite $\beta \colon Y^\prime \to Y$.

  Recall that for any quasicoherent $\MO_Y$-algebra $\mathcal A$, we have \[\Hom_Y(X,\Spec \mathcal A) = \Hom_{\MO_Y \strich \mathrm{Alg}}(\mathcal A,\pi_\ast \MO_X) \,,\] with $\phi_g \mapsfrom g$ and we have
  \begin{equation*}
    \begin{tikzcd}
      X \ar[rr,"\varphi_g"] \ar[dr] && \Spec A \ar[dl,"q"]  \\
      &Y
    \end{tikzcd}
  \end{equation*}
  such that $q_\ast(\MO_{\Spec A} \to {\varphi_g}_\ast \MO_X) = (g \colon \mathcal A \to \pi_\ast \MO_X)$ $(\ast)$.
  Here, apply to $\mathcal A = \pi_\ast \MO_X$:
  \begin{align*}
    \Hom_X(X,Y^\prime) &= \Hom(\pi_\ast \MO_X,\pi_ast \MO_X)  \\
    \alpha &\mapsfrom \id_{\pi_\ast\MO_X}
  \end{align*}
  By $(\ast)$, $\beta_\ast (\MO_{Y^\prime} \xrightarrow{\alpha_\ast} \alpha_\ast \MO_X) = (\id \colon \pi_\ast \MO_X \to \pi_\ast \MO_X)$.
  Since $\beta$ is affine, we have $\alpha_\ast \colon \MO_{Y^\prime} \to \alpha_\ast \MO_X$ is an isomorphism.
  Since $\pi$ is proper and $\beta$ is finite hence seperated, $\alpha$ is proper via cancellation theorem.
\end{proof}
\begin{theorem}[Zariski's main theorem, classical version]
  Let $\pi \colon X \to Y$ be a proper morphism of Noetherian schemes.
  Suppose that $X,Y$ are integral, $\pi$ is birational and $Y$ is normal.
  Then $\pi$ is $\MO$-connected, hence has connected fibers.
\end{theorem}
\begin{remark}
  Counterexample: normalization is birational and proper
\end{remark}
\begin{proof}
  Stein factorization:
  \begin{equation*}
    \begin{tikzcd}
      X \ar[drr,"\pi"'] \ar[rr,"\text{$\MO$-connected}","\alpha"'] && Y^\prime \ar[d,"\beta"',"\text{finite}"] \ar[r,symbol=\supseteq,"\text{open}"] & \Spec \MO_U  \ar[d] \\
      && Y \ar[r,symbol=\supseteq] & U \; .
    \end{tikzcd}
  \end{equation*}
  Let $U \subseteq Y$ be a dense open subset such that $\res{\pi^{-1}}{U}$ is defined (that is $\res{\pi}{\pi^{-1}(U)}$ is an isomorphism).
  Then $\res{\pi_\ast \MO_X}{U} \cong \pi_\ast (\res{\MO_X}{\pi^{-1}(U)})$, and therefore $\beta$ is birational (and finite).
  \begin{claim}
    $\beta$ is an isomorphism (therefore $\pi = \alpha$, then $\pi$ is $\MO$-connected, so $\pi$ has connected fibers)
  \end{claim}
  \begin{proof}
    Assume $Y =\Spec B$.
    $\beta$ finite, so $\beta$ is affine, so $Y^\ast = \Spec B^\prime$.
    $\beta$ is birational, so $\Frac B \cong \Frac B^\prime =: K$ ($B$, $B^\prime$ integral since $X$, $Y$ are integral by assumption).
    $\beta$ is finite, so $B^\prime$ is integrally dependent on $B$.
    As $Y$ is normal, $B$ is integrally closed, so $B = B^\prime$.
  \end{proof}
\end{proof}
\begin{theorem}[Zariski's main theorem, Grothendieck's version]
  Let $\pi \colon X \to Y$ be a seperated quasifinite (finite type over $k$ and every fiber is a discrete (finite) set)  morphism of Noetherian schemes.
  Then there exists a factorization
  \begin{equation*}
    \begin{tikzcd}
      X \ar[rr,hookrightarrow,"\text{open immersion}"]& & Y^\prime \ar[r,"\text{finite}"] & Y
    \end{tikzcd}
  \end{equation*}
  (this is the stein factorization if $\pi$ is proper).
\end{theorem}
\begin{proof}[Proof (sketch)]
  Take the Stein factorization
  \begin{equation*}
    \begin{tikzcd}
      X \ar[r,hookrightarrow, "\alpha"] & Y^\prime \ar[d] \\ & Y
    \end{tikzcd}
  \end{equation*}
  $\alpha$ is $\MO$-connected, so the fibers are connected, so every fiber of $\alpha$ is a single point.
  Then show that $\alpha$ is an open immersion, see Vakil.
\end{proof}
\begin{corollary}
  Proper and quasi-finite implies finite.
\end{corollary}
\begin{theorem}[Zariski's main theorem, our version]
  Let $\pi \colon X \to Y$ be a proper morphism of Noetherian schemes, $\pi$ birational, $X$, $Y$ integral, $Y$ normal.
  If $\pi$ does not contract a curve (e.q. every fiber is $0$-dimensional), then $\pi$ is an isomorphism.
\end{theorem}
\begin{proof}
  Consider the factorization of Grothendieck's version
    \begin{equation*}
    \pi \colon \begin{tikzcd}
      X \ar[rr,hookrightarrow,"\text{open immersion}"] && Y^\prime \ar[r,"\beta"',"\text{finite}"] & Y
    \end{tikzcd}
  \end{equation*}
  As $\pi$ is proper, $\alpha$ is proper, so $\alpha$ is an isomorphism, so $\pi = \beta$ is finite.
  The claim of the proof of the classical version gives us that as $\pi$ is birational, finite, and $Y$ is normal, $\pi$ is an isomorphism.
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeo_master"
%%% End:
