* What is this?
The lecture notes of the courses Algebraic Geometry I and Algebraic Geometry II held by Georg Oberdieck in the winter semester 18/19 and the summer semester 19 at the Rheinische Friedrich Wilhelms Universitaet in Bonn.
* Where is the PDF?
[[https://zmberber.gitlab.io/algebraic-geometry-oberdieck/alggeo_master.pdf][Here]].
* FAQ
** How was it compiled?
With the command ~latexmk -pdflatex=lualatex -pdf alggeo_master.tex~ while in this directory.
** How can I contribute?
The best way to contribute is to clone this repository and issue a merge request or a pull request.
Email Berthold at ~berthold@lorke.de~ or Branko at ~[missing]~ for any comments you would like to give us.
** Why are these notes so shitty?
Firstly, sometimes the structure of the lecture is done spontaneously by Prof. Oberdieck, so we are not sure how to categorize his notes.
Secondly, we are sometimes lazy and just write down what we see on the board and upload it once, without reviewing it that often.
Since we write the LaTeX code while in class, we do not always have time to do things the "right" way in LaTeX right away, and we are usually not in the mood to make it prettier later.
Sometimes Branko's English is also kind of wonky.