\begin{proposition} \label{MPV}
	Let $X, Y$ be prevarities and $f \colon X \to Y$ any map. Let $V_i$ be an open cover of $Y$ and $V_i$ be affine.
	
	Let $U_i$ be an open cover of $X$ such that 
	\begin{enumerate}[label=(\roman*)]
		\item $f(U_i) \subset V_i$
		\item $f$ induces $f^* \colon \mathcal O_Y(V_i) \to \mathcal O_X(U_i)$
	\end{enumerate}
	Then, $f$ is a morphism.
	\begin{proof}
		The map $f$ is a morphism iff it is continuous and $f^*$ induces a map $\mathcal O_Y \to f_*\mathcal O_X$.
		
		\textbf{Step 1:} Reduce to the case where $U_i$ are affine varieties. 
		
		Let $U_i \subset X$ be open and let $U_i$ be covered by open affine subvarieties $U_{ij}$.
		
		We know $f(U_{ij}) \subset V_i$. 
		Let $g \in \mathcal O_Y(V_i)$, then \[\res{f}{U_{ij}}^\ast(g)=\res{\res{f}{U_i}^\ast(g)}{U_{ij}}.\]
		Therefore, we get \[\res{f}{U_{ij}}^\ast \colon \mathcal O_Y(V_i) \to \mathcal O_X \,.\]
		
		Replacing $\{(U_i,V_i)\}$ by $\{(U_{ij},V_j)\}$ we can assume that $U_i$ is affine.
		
		\textbf{Step 2:} Set $F=\res{f}{U_i} \colon U_i \to V_i$ where $U_i \subset k^m$ and $V_i \subset k^n$ are closed. The map $F$ is a morphism.
		
		By assumption: $F^\ast \colon \mathcal O_{V_i}(V_i) \to \mathcal O_{U_i}(U_i)$.
		Using \Cref{ICR}, we know that $\mathcal O_{V_i}(V_i) = k[y_1, \dots, y_n]/I(V_i)$ and $\mathcal O_{U_i}(U_i)  = k[x_1, \dots , x_m]/I(U_i)$.
		Let $\overline F_l = F^\ast(y_l)$ and lift $\overline F_l$ to $F_l \in k[x_1, \dots , x_m]$.
		Then, $F=(F_1, \dots , F_n)$ is a morphism of affine algebraic sets which implies that $F$ is a morphism of prevarieties. 
		
		\textbf{Step 3:} We know that $X$ covered by $U_i$ such that $\res{f}{U_i}$ is a morphism. We will check that $f$ is a morphism.
		
		The map $f$ is continuous. Let $W \subset Y$. We know that 
		\begin{align*}
			f^{-1}(W)=\bigcup_i f^{-1}(W) \cap U_i = \bigcup_i \left(f|_{U_i}\right)^{-1}(V_i \cap W)
		\end{align*}
		which is open as a union of open sets.
		
		Furthermore, every pullback of a regular function is regular.
		Let $g \in \mathcal O_Y(W)$ and $f^\ast(g) \colon f^{-1}(W) \to k$. 
		We can compute \[\res{f^\ast(g)}{U_i \cap f^{-1}(W)}=(\res{f}{U_i})^\ast(\res{g}{V_i \cap W})\] where $\res{g}{V_i \cap W} \colon V_i \cap W \to k$ with $\res{g}{V_i \cap W} \in \mathcal O_Y(V_i \cap W)$.
		
		By Step 2, this implies that $\res{f^\ast(g)}{U_i \cap f^{-1}(W)} \in \MO_X(U_i \cap f^{-1}(W))$. 
		This implies $f^\ast(g) \in \MO_X(f^{-1}(W))$ since $\MO_X$ is sheaf (gluing property). 
	\end{proof}
	\begin{note}
		What if $V_1$ is not affine?
		If $V_1 = \mathbb A^1 \setminus \{0\}$ just consider the inclusion into $\mathbb A$ and everything works similar. 
		
		The problem comes when setting $V_1 = \PR^n$ which leads to $\mathcal O_Y(V_i)=k$ (constant maps) and the two conditions will always be true.
	\end{note}
\end{proposition}
\begin{example}
	Consider $zy^2=x(x^2-z^2)$ with vanishing set $C$ in $\PR_k^2$. Let $U_z=\{[x,y,z] \in \PR_k^2 \mid z \neq 0\}$ and consider the isomorphism 
	\begin{align*}
	\psi \colon \qquad U_z & \xrightarrow{\cong} \, k^2 \\
	[x,y,z] & \mapsto (x/z,y/z)
	\end{align*}
	which implies $Y^2=X(X^2-1)$ in local coordinates $(X,Y)\in k^2$.
	\begin{figure}[h]
		\centering
		\begin{tikzpicture}
		\begin{axis}[
		xmin=-1,
		xmax=3,
		ymin=-3,
		ymax=3,
		xlabel={$X$},
		ylabel={$Y$},
		axis equal,
		yticklabels={,,},
		xticklabels={,,},
		axis lines=middle,
		samples=200,
		smooth,
		clip=false,
		]
		\addplot [thick, domain=-1:0] {sqrt(x^3-x)};
		\addplot [thick, domain=-1:0] {-sqrt(x^3-x))};
		\addplot [thick, domain=1:2] {sqrt(x^3-x)};
		\addplot [thick, domain=1:2] {-sqrt(x^3-x))};
		\end{axis}
		\end{tikzpicture}
		\caption{Solutions of $Y^2=X(X^2-1)$ in $\R^2$}
	\end{figure}
	
	There is one point in $C$ which is not in $U_z$: $P_\infty = [0,1,0]$.
	
	We can intepret this as the slice of a torus.
	
	\begin{figure}[h]
		\begin{tikzpicture}
		\draw (-3.5,0) .. controls (-3.5,2) and (-1.5,2.5) .. (0,2.5);
		\draw[xscale=-1] (-3.5,0) .. controls (-3.5,2) and (-1.5,2.5) .. (0,2.5);
		\draw[rotate=180] (-3.5,0) .. controls (-3.5,2) and (-1.5,2.5) .. (0,2.5);
		\draw[yscale=-1] (-3.5,0) .. controls (-3.5,2) and (-1.5,2.5) .. (0,2.5);
		
		\draw (-2,.2) .. controls (-1.5,-0.3) and (-1,-0.5) .. (0,-.5) .. controls (1,-0.5) and (1.5,-0.3) .. (2,0.2);
		
		\draw (-1.75,0) .. controls (-1.5,0.3) and (-1,0.5) .. (0,.5) .. controls (1,0.5) and (1.5,0.3) .. (1.75,0);
		
		\draw[very thick,dotted] (2.75,0) circle (0.75);
		\draw[very thick,dotted] (-2.75,0) circle (0.75);
		
		\end{tikzpicture}
		\centering
		\caption{As a slice of a torus}
	\end{figure}
	
	Let $P_0=[0,0,1] \in C$. We want to define a map $\varphi$ describing a projection via a line through $P_0$.
	\begin{align*}
	\varphi \colon \qquad C & \longrightarrow C \\
	P & \longmapsto Q & \text{ where } L \cap C = \{P_0 , P , Q\} \\
	P_0 & \longmapsto P_\infty \\
	P_\infty & \longmapsto P_0
	\end{align*}
	with $L$ being a line.
	
	\begin{figure}[h]
		\centering
		\begin{tikzpicture}
		\begin{axis}[
		xmin=-1.5,
		xmax=2.5,
		ymin=-3,
		ymax=3,
		xlabel={$X$},
		ylabel={$Y$},
		axis equal,
		yticklabels={,,},
		xticklabels={,,},
		axis lines=middle,
		samples=200,
		smooth,
		clip=false,
		]
		\addplot [thick, domain=-1:0] {sqrt(x^3-x)};
		\addplot [thick, domain=-1:0] {-sqrt(x^3-x))};
		\addplot [thick, domain=1:2] {sqrt(x^3-x)};
		\addplot [thick, domain=1:2] {-sqrt(x^3-x))};
		\addplot [thick, domain=-1.5:2.5] {0.3*x};
		\end{axis}
		\draw (2.95,2.85)  node[below right]{$P_0$};
		\draw (1.6,2.5) node[below right]{$P$};
		\draw (3.7,3.4) node{$Q$};
		\end{tikzpicture}
		\caption{The line $L$ through $P, Q, P_0$}
	\end{figure}
	Now, we will describe this in coordinates.
	Define $C_z = C \cap U_z$ and $C_x,C_y$ similarly. 
	We have $L=\setdef{(at,bt)}{t \in k}$ which means that $L \cap C$ are solutions of \[b^2t^2 = at((at)^2-1) \Longrightarrow t(t-1)(a^2t+1)=0\,.\]
	The first root is referring to $P_0$, the second one to $P$. The third one is referring to $Q=\left(-\frac{1}{a},{-\frac b{a^2}}\right)$. 
	
	Therefore, $\varphi$ on $C_z \cap C_x$ should be given by $(X,Y) \mapsto \left( -1/X,-Y/X^2 \right)$ (using local coordinates from $k^2 \cong C_z$). For $P_0$ and $P_\infty$, $\varphi$ is given by $\varphi(P_\infty)=P_0$ and $\varphi(P_0)=P_\infty$.
	
	We now formally defined a map $\varphi \colon C \to C$. We will use \Cref{MPV} for showing that this is actually morphism. 
	Let 
	\begin{itemize}
		\item $U_1=C_z \cap C_x=C \setminus \{P_0,P_\infty\}=k^2 \setminus 0$ with $V_1=U_z$
		\item $U_2=C_y=C \setminus \{P_0,\psi^{-1}(-1,0),\psi^{-1}(1,0)\}$ with $V_2=C_z$ and
		\item $U_3=C \setminus \{P_\infty, \psi^{-1}(1,0),\psi^{-1}(-1,0)\}$ with $V_3=C_y$
	\end{itemize}
	
	We have $\res{\varphi}{U_1} \colon U_1 \to V_1=C_z$ which can already be described in local coordinates. We have \[\mathcal O_C(C_z)=k[X,Y]/(Y^2-X(X^2-1))\] which is generated by $X,Y$. We only need to check that the pullback of $X$ and $Y$ are regular.
	
	We have 
	\begin{align*}
	\left(\res{\varphi}{U_1}\right)^*(X)&=-\frac{1}{X} \\
	\left(\res{\varphi}{U_1}\right)^*(Y)&=-\frac{Y}{X^2}
	\end{align*}
	which are regular on $U_1$.
	
	Furthermore, consider $\res{\varphi}{U_2} \colon U_2 \to V_2$. Applying the formula for $U_1$ to $[(c,1,d)] \in U_2$ we would get
	\begin{align*}
	\varphi([(c,1,d)])&=\varphi\left(\left[\frac{c}{d},\frac{1}{d},1\right]\right) \\
	& = \left[-\frac{d}{c^2}, - \frac{d^2}{c^2} \cdot \frac{1}{d}, 1\right] \\
	& = \left[-\frac{d}{c^2}, - \frac{d}{c^2}, 1\right]
	\end{align*} 
	or using local coordinates from \begin{align*}   U_2 & \longrightarrow k^2 \\  [x,y,z] & \longmapsto (x/y,z/y) \end{align*}
	for $(S,T) \in k^2$ we should have
 	\[\varphi(S,T)=\left(-\frac{T}{S}, - \frac{T}{S^2}\right)\] which is unfortunately not defined at $S=0$.
	
	But  $zy^2=x(x^2-z^2)$ translates into $T=S(S^2-T^2)$.% or in previous notation $d=c(c^2-d^2)$. 
	
	%In this case: Consider $\\varphi|_{U_1} \colon U_1 \to V_1$ where $\varphi|_{U_i}(a,b)=\left(-\frac1a, - \frac{b}{a^2}\right)$ and note that $\varphi^*|_{U_1}X=-\frac{1}{X}$ and $\varphi^*|_{U_2}Y=-\frac{Y}{X^2}$ are regular on $U_1 =k^2 \setminus 0$.
	
	So, we can also write
	\begin{align*}
	\varphi(c,d)&=\left(-\frac{T}{S}, - \frac{T}{S^2}\right) \\
	& = \left( -(S^2-T^2),-S+T(S^2-T^2) \right).
	\end{align*}
	where the coordinates in the domain are the ones from $C_z$.
	This defines $\varphi$ on all $U_2$ (Note that $P_\infty=[0,1,0]=(0,0)$ in $U_2$).
	
	All in all, using coordinates $S,T$ for $U_2$ and $X,Y$ for $V_2$, we have $f^\ast X=-(S^2-T^2)$ and $f^*Y=-S+T(S^2-T^2)$ which are regular functions.
	% where \begin{align*} X \colon U_z \cap C & \longrightarrow k \\ S \colon U_y \cap C & \rightarrow k \end{align*} 
	
	All in all, $\res{\varphi}{U_2} \colon U_2 \to V_2$ is a morphism. 
	
	The chart for $U_3$ is exercise. 
	
	Now, we can actually apply \Cref{MPV} and $\varphi$ is a morphism.
\end{example}
\begin{note}
	One can also consider the equation $zy^2=x(x-z)(x- \lambda z)$ for $\lambda \in k$. The case $\lambda = -1$ is the one above. 
	
	The case $\lambda = 1$ is also very interesting.
	\begin{figure}[h!]
		\centering
		\begin{tikzpicture}
		\begin{axis}[
		xmin=-1,
		xmax=3,
		ymin=-3,
		ymax=3,
		xlabel={$X$},
		ylabel={$Y$},
		axis equal,
		yticklabels={,,},
		xticklabels={,,},
		axis lines=middle,
		samples=200,
		smooth,
		clip=false,
		]
		\addplot [thick, domain=0:3] {sqrt(x*(x-1)*(x-1))};
		\addplot [thick, domain=0:3] {-sqrt(x*(x-1)*(x-1))};
		\end{axis}
		\end{tikzpicture}
		\caption{Solutions of $Y^2=X(X^2+1)$ in $\R^2$ (Case $\lambda = 1$)}
	\end{figure}
\end{note}
\section{Products}
\begin{definition}[products] \label{DefProd}
	Let $\mathcal C$ be a category and $X,Y \in \obj(\mathcal C)$. An object $Z$ with morphisms $P: Z \to X$ and $q: Z \to Y$ is a \textbf{product of $X$ and $Y$} if for all $W \in \mathcal C, f \colon W \to X, g \colon W \to Y$ there uniquely exists $t \colon W \to Z$ such that $p \circ t = f$ and $q \circ t = g$.
	$$\begin{tikzcd}
	& X \\
	W \arrow{ur}{\forall f} \arrow{dr}[swap]{\forall g} \arrow[dotted]{r}{\exists !} & Z \arrow{u}[swap]{p} \arrow{d}{g} \\
	& Y
	\end{tikzcd}$$ 
	
	If $Z$ exists then it is unique (up to isomorphism). We write $Z = X \times Y$. 
\end{definition}
If the product $X \times Y$ exists, then 
\[
|X \times Y|_{\Set}=\Hom(*, X \times Y) = \Hom(*,X) \times \Hom(*,Y) = |X|_{\Set} \times |Y|_{\Set}\,.\]
\begin{proposition} \label{HEP}
	Let $X,Y$ be affine varieties. Then 
	\begin{enumerate}[label=(\alph*)]
		\item There exists a product prevariety $X \times Y$.
		\item $X \times Y$ is affine with coordinate ring $k[X \times Y]=k[X] \otimes_k k[Y]$.
	\end{enumerate}
	\begin{proof}
		Let $X \subset k^m, X=V(f_1, \dots , f_{m_1})$ and $Y \subset k^n, Y=V(g_1, \dots , g_{m_2})$.
		We will show that $X \times Y$ (as a set) is the zero locus of $f_1, \dots , f_{m_1}, g_1, \dots , g_{m_2}$.
		
		Define $X \times Y=V(f_1, \dots , f_{m_1},g_1, \dots , g_{m_2})$ as a prevariety. 
		First of all, we have \[k[x_1, \dots , x_m, y_1, \dots , y_n]/(f_i,g_j) \cong k[x_1, \dots, x_m]/(f_i) \otimes_k k[y_1, \dots , y_n]/(g_j) = k[X] \otimes_k k[Y]\,.\]
		Since $X,Y$ are affine varities, we know that $k[X], k[Y]$ are integral domains which implies that their tensor product is an integral domain. 
		This implies that $(f_i,g_j)=I(X \times Y)$ is a radical prime ideal. 
		$X \times Y$ is defined by a prime ideal, so it is irreducible and therefore an affine variety. 
		
		Now, we need to check that $X \times Y$ is a categorical product.
		
		The projections $p \colon X \times Y \to X$ and $q \colon X \times Y \to Y$ are given by the projection on coordinates.
		
		Let $f,g$ given as in \Cref{DefProd}. Since $|X \times Y|=|X| \times |Y|$, there is an induced map of sets $t \colon W \to X \times Y$. We need to check that this map is a morphism. 
		
		Set $V_1 = X \times Y$ and $U_1 = W$ and use \Cref{MPV}. The ring $k[X \times Y]$ is generated by $p^*k[X]$ and $q^*k[Y]$ and we actually have $t^*p^*k[X \times Y]= f^*k[X] \subset \mathcal O_W(W)$.
	\end{proof}
\end{proposition}
\begin{theorem} \label{EP}
	The category of prevarieties has a product.
\end{theorem}
\begin{proof}
	Let $X,Y$ be prevarities. 
	
	\textbf{Step 1:} Endow $X \times Y$ with a topoligcal and a structure sheaf. 
	
	\textbf{Topology:} Define a basis $\mathcal B$ of a topology by
	\[\left\{
		 W \subset X \times Y \,\middle|\, \begin{matrix*}[l] \exists U \subset X \text{ affine}, V \subset Y \text{affine}: W \subset U \times V,  \\ W=D\left(\sum f_ig_i\right) , f_i \in k[U], g_i \in k[V]
	\end{matrix*} \right\}.\]
	
	Let $D\left(\sum f_ig_i\right) \subset U \times V$ and $D\left(\sum f_i'g_i'\right) \subset U' \times V'$. We have that \[D\left(\sum f_ig_i\right) \cap D\left(\sum f'_ig'_i\right) \subset (U \cap U') \times (V \cap V')\] which can be writen as $U_i'' \times V_I''$ 
	
	Furthermore, \[D\left(\sum f_ig_i\right) \cap D\left(\sum f'_ig'_i\right) \cap U''_i \times V''_i = D\left(\left(\sum f_ig_i\right)|_{U''_i \times V''_i} \cdot \left(\sum f'_ig'_i\right)|_{U''_i \times V''_i}\right) \in \mathcal B\]
	
	Now we need to define the structure sheaf. Let $W \in \mathcal B$ and set \[\mathcal O_{X \times Y}(W) := \mathcal O_{U \times V}(W)\]
	
	Let $\mathcal O_{X \times Y}$ be te induced sheaf on $X \times Y$. The construction of this will be given after this proof (\Cref{ExIndSh}).
	
	We need to check that $(X \times Y, \mathcal O_{X \times Y})$ is a categorical product. 
	
	First of all, $X\times Y$ is a prevariety: It is connected and covered by the affine varieties $U \times V$. 
	
	Now we need to check the universal property. Let $f \colon W \to X$ and $g \colon W \to Y$. There is a map of sets $t \colon W \to X \times Y$. 
	We know that $t^{-1}(U \times V)=f^{-1}(U) \cap g^{-1}(V)$ which is open. Consider 
	\[\begin{tikzcd}
	& & U \\
	t^{-1}(U \times V)  \arrow{urr}{f} \arrow{rr}[swap]{\res{t}{t^{-1}(U \times V)}} \arrow{drr}[swap]{g} & & U \times V \arrow{u} \arrow{d} \\
	& & V
	\end{tikzcd}\]
	Observe that $\res{t}{t^{-1}(U \times V)}$ is the induced map from $\res{g}{t^{-1}(U \times V)}$ and $\res{f}{t^{-1}(U \times V)}$. All in all, $\res{t}t^{-1}(U \times V)$ is a morphism by \Cref{HEP}.
\end{proof}