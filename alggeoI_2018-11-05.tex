\section{Function Field}
Let $X$ be a variety (or prevariety).
\begin{lemma} \label{dense}
	If $f, g \colon X \to k$ are regular and there exists a nonempty open $U \subset X$ such that $\res{f}{U}=\res{g}{U}$, then $f=g$.
\end{lemma}
\begin{proof}
	Let $Z=\setdef{x \in X}{f(x)=g(x)}=(f-g)^{-1}(0)$ which is closed. Since $X$ is irreducible, $U$ is dense and therefore $Z$ is dense which implies $Z=X$.
\end{proof}
\begin{definition}[function field]
	The \textbf{function field} of $X$ is \[k(X)=\setdef{(U,f)}{U \subset X \text{ nonempty, open},f \colon U \to k \text{ regular}}/\sim\] where $(U,f) \sim (V,g) \iff \exists W\neq \emptyset \text{ open in } U \cap V$ such that $\res{f}{W}=\res{g}{W}$.
	
	Equivalently, we can define \[k(X)=\varinjlim\limits_{U \in I} \MO_X(U)\] where $I=\set{U \subset X \text{ nonempty, open}}$ with $U \ge V \iff U \subset V$ and $U \mapsto \mathcal O_X(U)$ plus restriction maps being an inductive system.
\end{definition}
\begin{remark}
	As a consequence of \Cref{dense} we have that 
	\begin{align*}
		\MO_X(U) & \longrightarrow k(X) \\
		f& \longmapsto [(U,f)]
	\end{align*}
	is injective
\end{remark}
\begin{proof}
	Let $f,g \in \MO_X(U)$ such that $[(U,f)]=[(U,g)]$. Then there exists $W \subset U$ such that $\res{f}{W}=\res{g}{W}$ which implies $f=g$ by \Cref{dense}.
\end{proof}
\begin{remark}
	$k(X)$ is a field. 
	
	\textbf{Addition:} We define \[(U,f)+(V,g) \coloneqq (U\cap V, \res{f}{U \cap V}+\res{g}{U \cap V}) \,.\]
		
	\textbf{Multiplication:} Similar
	
	The multiplicative inverse can be obtained as follows: Let $(U,f)$ with $f \neq 0$. Then, $(U \cap D(f),( \res{f}{D(f)})^{-1})$ is the multiplicative inverse to $(U,f)$. 
\end{remark}
\begin{remark}
	Let $x \in X$ and let $\MO_{X,x}$ be the stalk of $\MO_X$ at $x$. Then, \[\MO_{X,x} \to k(X)\] is injective by \Cref{dense}.
	
	Therefore, we can also define \[\MO_X(U)= \bigcap_{x \in U} \MO_{X,x} \, .\]
\end{remark}
\begin{remark}
	Let $X$ be an affine variety. Let $\MO_X(X)=k[X]=R$. We know that $\MO_X(D(f))=R_f$, the localization of $R$ at $f$.
	
	Furthermore, $\setdef{D(f)}{f \neq 0}$ is a basis of the topology. 
	
	All in all, \[k(X)=\varinjlim\limits_{k[X]\ni f \neq 0} \MO_X(D(f))=\varinjlim\limits_{k[X] \ni f \neq 0} R_f=\bigcup_{k[X] \ni f \neq 0} \setdef{\frac{a}{f^k}}{a \in R}/\sim\] where $\frac{a}{f^k}\sim \frac{b}{g^l} \iff \exists h \in R$ such that $f|h$ and $g|h$ such that $\frac{a}{f^k}= \frac{b}{g^l}$ in $R_h=\MO_X(D(h))$ which is $\iff$ there exists $m$ such that $h^m (ag^l-bf^k)=0$ in $R$. All in all, \[k(X) \cong \Frac(R)\,.\]
\end{remark}
\begin{remark}
	Let $X$ be a variety and $U$ be open, nonempty. Then $k(X) \cong k(U)$. In particular, if $U$ is affine, then $k(X)=\Frac(R)$ where $R=\mathcal O_X(U)$ is a finitely generated $k$-algebra and $k(X)$ a finitely generated field extension of $k$.
\end{remark}
\begin{lemma}
	Let $X$ be a (pre-)variety and $Y$ be a variety.
	Let $f,g \colon X \to Y$ be morphisms such that $$\res{f}{U}=\res{g}{U}$$ for a nonempty open $U \subset X$. Then $f=g$
\end{lemma}
\begin{proof}
	We know that $Z=\setdef{x \in X}{f(x)=g(x)}$ is closed since $Y$ is a variety. Rest as before.
\end{proof}
\begin{definition}[rational map]
	 Define \[\RatMap(X,Y)= \setdef{(U,f)}{U \subset X \text{ nonempty, open}, f \colon U \to Y \text{ morphism}}/\sim\] where $(U,f) \sim (V,g)$ $\iff$ $\exists W \subset U \cap V$ such that $\res{f}{W}=\res{g}{W}$ the \textbf{set of rational maps}.

	An equivalence class $(U,f) \in \RatMap(X,Y)$ is called a \textbf{rational map from $X$ to $Y$}, denoted by \[f \colon X \dashrightarrow Y\,.\]
	The rational map $f$ is \textbf{dominant} if $f(U)$ is dense in $Y$ (for some $U \subset X$ or equivalently for every domain of definition $U \subset X$)
	
	(For $U \subset X$ open, nonempty with $f(U) \subset Y$ dense and $V \subset U$, we have $f(V) \subset Y$ dense.)
\end{definition}
\begin{remark}
	Let $f \colon X \dashrightarrow Y$ be dominant rational map and $g \colon Y \dashrightarrow Z$ be any rational map where we can consider $f,g$ as representatives of $f \colon U \to Y$ and $g \colon V \to Z$ with $U \subset X, V \subset Y$. Since $f$ is dominant, $f(U) \cap V \neq \emptyset$ which implies $f^{-1}(V) \neq \emptyset$.
	
	Define the composition $g \circ f \colon X \dashrightarrow Z$ by $(f^{-1}(V), g \circ f)$. 
\end{remark}
\begin{definition}[birational]
	A rational map $f \colon X \dashrightarrow Y$ is \textbf{birational} if it is dominant and there exist a dominant $g \colon Y \dashrightarrow X$ such that $f \circ g \cong \id_Y$ and $g \circ f \cong \id_X$ (as rational maps in $\RatMap(X,X)$ or $\RatMap(Y,Y)$, respectively).
	
	We say that \textbf{$X$ and $Y$ are birational} if there exists a birational map $X \dashrightarrow Y$. 
\end{definition}
\begin{proposition}
	$X$ and $Y$ are birational $\iff$ $k(X) \cong k(Y)$
\end{proposition}
\begin{proof}
	Exercise
\end{proof}
\begin{proposition}
	There exist a contravariant equivalence between 
	\begin{enumerate}
		\item Categories of varieties and dominant rational maps
		\item Category of finitely generated field extensions over $k$
	\end{enumerate}
\end{proposition}
\begin{example}
	The varieties $X=\PR^1$ and $Y=\A^1$ are birational because $\A^1$ can be considered as an open subset of $\PR^1$. But they are not isomorphic since we have $\MO_{\A^1}(\A^1)=k[x]$ but $\MO_{\PR^1}(\PR^1)=k$.
\end{example}
\begin{remark} $X$ and $Y$ are birational iff there exists nonempty open $U \subset X, V \subset Y$ such that $(U, \res{\MO_X}{U}) \cong (V, \res{\mathcal O_Y}{V})$ 
	\begin{figure}[h!] \label{y^2=x^2(x+1)}
		\centering
		\begin{tikzpicture}
		\begin{axis}[
		xmin=-1,
		xmax=3,
		ymin=-3,
		ymax=3,
		xlabel={$x$},
		ylabel={$y$},
		axis equal,
		yticklabels={,,},
		xticklabels={,,},
		axis lines=middle,
		samples=200,
		smooth,
		clip=false,
		]
		\addplot [thick, domain=-1:2] {sqrt(x^3+x^2)};
		\addplot [thick, domain=-1:2] {-sqrt(x^3+x^2))};
		\end{axis}
		\end{tikzpicture}
		\caption{$y^2=x^2(x+1)$}
	\end{figure}	
	For example, we can consider $y^2=x^2(x+1)$ which is birational to $\PR^1$ (We say $C$ is rational). (Sheet 3, Problem 1).

	Another example is $y^2=(x+1)x(x-1)$ which is not rational to $\PR^1$ (\Cref{y^2=x^2(x+1)}, later).
\end{remark}
\section{Dimensions}
\begin{definition}[dimension]
	Let $X$ be a variety. Define \[\dim X \coloneqq \tr \deg_k k(X)\]
	
	($\dim X =n \iff \exists $ algebraically independent $\zeta_1, \dots , \zeta_n \in k(X)$ such that $k(X)$ is an algebraic extension over $k[\zeta_1, \dots , \zeta_n]$)
\end{definition}
\begin{remark} % originally after definition of finite morphism
	For a ring $A$, the krull dimension of $A$ is defined as \[\operatorname{dim}A = \operatorname{sup}\setdef{r}{p_0 \subsetneq \dotsb \subsetneq p_r \subseteq A, p_i \text{ prime}} \, .\]
	We have $\dim X = \dim k[X]$.
\end{remark}
\begin{proposition} \label{dim<}
	Let $Y \subsetneqq X$ be a proper closed subvariety. Then $\dim Y < \dim X$.
\end{proposition}
\begin{proof}
	Let $U \subset X$ be affine with $U \cap Y \neq \emptyset$. Then, $k(X) = k(U)$ which implies $\dim X = \dim U$ and similarly $\dim Y = \dim (U \cap Y)$. 
	
	Therefore, we can assume that $X$ and $Y$ are affine. In particular, $Y=V(\pid)$ where $\pid \subset k[X]$ is prime. Let $R=k[X]$ and $k[Y]=R/\pid$.
	
	If $\dim Y = \dim X=n$, then there exists $\overline \zeta_1, \dots \overline \zeta_n \in R/\pid$ algebraically independent. Lift these two $\zeta_1, \dots , \zeta_n \in R$. Let $y \in \pid$ be nonzero. 
	
	There exists $P(X_1, \dots , X_n,Y) \neq 0$ with coefficients in $k$ such that $P(\zeta_1, \dots , \zeta_n, y)=0$. This implies $P(\overline \zeta_1, \dots , \overline \zeta_n, 0)=0$ in $R/\pid$.
	
	$R$ is an integral domain, therefore we can assume that $P$ is irreducible. 
	There are two cases
	\begin{enumerate}
		\item The constant term in $Y$ is nonzero ($P(X_1, \dots , X_n,0) \neq 0$), then we are done.
		\item Otherwise, we have $P=aY$ for some $a \neq 0$. But this implies $P(\zeta_1, \dots , \zeta_n,y)=ay \neq 0$ which is a contradiction. \qedhere
	\end{enumerate}
\end{proof}
\begin{definition}[topological dimension]
	Let $X$ be a topological space. Define \[\topdim X \coloneqq \sup \setdef{r}{\exists \, \text{chain }Z_0 \subsetneq \dots\subsetneq  Z_r \subset X, Z_i \text{ irreducible, closed}} \, .\]
\end{definition}
Let $X$ be a variety and $Z_0 \subset Z_i \subset \dots \subset Z_r=X$ be a chain. By \Cref{dim<}, we have that $\dim Z_{i+1} - \dim Z_i \ge 1$. Therefore, $\dim X = \dim Z_r \ge r$. Taking $\sup$ leads to $\dim X \ge \topdim X$. 

Let $Z_0 \subset \dots \subset Z_r = X$ be a maximal chain. Can we have $\dim Z_{i+1} - \dim Z_i > 1$?
\begin{theorem}[Krull Principal Ideal Theorem] \label{Krull}
	Let $X$ be a variety, $f \in \MO_X(X)$ nonzero. Let $Z$ be irreducible component of $V(f)$. Then, \[\dim Z = \dim X -1\,.\]
\end{theorem}
We will prove this later because we first need to introduce some new techniques. 
Nevertheless, let us first look at a consequence of this theorem. 
\begin{corollary}
	Let $X$ be a variety. We have \[\dim X = \topdim X \, .\]
\end{corollary}
\begin{proof}
	If suffices to prove the following: Let $Z \subset X$ be maximal proper irreducible closed subset. Then $\dim Z = \dim X-1$. 
	
	Assume that $X$ is affine. Then $Z=V(\ai)$ with $\ai \subset k[X]$. Then there exists $f \in k[X]$ vanishing on $Z$. 
	
	Let $Z^\prime$ be irreducible component of $V(f)$ that contains $Z$. 
	By maximality: $Z = Z^\prime$. 
	Therefore, $\dim Z = \dim X -1$ by \Cref{Krull}.
\end{proof}
\begin{definition}[finite morphism]
	Let $X,Y$ be affine varieties and $f \colon X \to Y$ be a morphism. Then $f^* \colon k[Y] \to k[X]$. The morphism $f$ is called \textbf{finite} $\iff$ $k[X]$ is integrally closed over $f^*k[Y]$.
\end{definition}
%\begin{proposition}
%	Let $f \colon X \to Y$ finite. Then 
%	\begin{enumerate}
%		\item $f$ is closed (i.e. $f(Z)$ closed for all closed $Z \subset X$).
%		\item For all $y \in Y$ we have that $f^{-1}(y)$ is finite.
%		\item $f$ surjective $\iff$ $f^*$ injective. 
%	\end{enumerate}
%\end{proposition}
%\begin{proof}  Let $\ai^\prime = (f^*)^{-1}(\ai)$. We will show that $f(V(\ai))=V(\ai^\prime)$.
	
%	Let $\mi \subset k[X]$ be a maximal ideal such that $\mi \supset \ai$.
%	But this leads to $(f^*)^{-1}(\mi) \supset (f^*)^{-1}(\ai)$. 
%	All in all, $f(V(\ai)) \subset V(\ai^\prime)$. 
	
%	Conversely, let $\tilde \mi \subset k[Y]$ be maximal ideal that contains $\ai^\prime$.
%	Consider $k[Y]/\ai^\prime \hookrightarrow k[X]/\ai$. By the Going Up Theorem $\tilde \mi/\ai^\prime = \mi^\prime \cap k[Y]/\ai$ for some $\mi^\prime \subset k[X]/\ai$.
	
%	This leads to $\tilde \mi = (f^*)^{-1}(\mi)$ where $\mi = \mi^\prime + \ai \subset k[X]$ which finally also proves $V(\ai^\prime) \subset f(V(\ai))$.
%\end{proof}
\begin{example}~ \vspace{-\topsep}
	\begin{itemize}
		\item Let $C=V(y^2-x^{50})$. Then $C \to \A^1, (x,y) \mapsto x$. This is finite.
		\item Let $C=V(xy)$ and consider $C \to \A^1, (x,y) \mapsto x$. This is not finite
	\end{itemize}
\end{example}